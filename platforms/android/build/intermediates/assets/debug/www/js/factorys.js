angular.module('starter.factories', [])

.factory('server', function($http,$rootScope,$localStorage,$timeout,$q,$state) {
    return {
        request: function (url, request) {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
            var deferred = $q.defer();
			var requestSended = '';
			if (request || request!= ''){
				requestSended =  JSON.stringify(request);
			}
			
			var httpDetails = 
			{
				url: $rootScope.Host + '/'+ url,
				method: 'POST',
				data: requestSended,
				contentType: "application/json"	
			};
			
		
            $http(httpDetails).
            success(function (json) {
				//console.log("POST : " , json);
                deferred.resolve(json);
            }).
            error(function (err) {
				alert("ERR : " + JSON.stringify(err));
                deferred.reject(err);
            });
            return deferred.promise;
        },
    }
} )







.factory('GetCustomersFactory', function($http,$rootScope,$localStorage) {
	var data = [];

	return {
		loadCustomers: function(params){
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
		/*	data1 = 
			{
				"categoryId" : $localStorage.custumerid 
			}				
			return  $http.post($rootScope.Host+'/getCategory.php',data1)
			.success(function(data, status, headers, config)
			{
					$rootScope.catArray = data;
					console.log("catagories: " , data)
					$scope.catArray = $rootScope.catArray;
					$scope.updateBasket();
					$rootScope.CatagoriesLoaded++;
			});		  */
			
			data1 = 
			{
				"categoryId" : $localStorage.custumerid
			}	
		
			return $http.post($rootScope.Host+'/getCategory.php',data1).then(function(resp)
			{
				$rootScope.catArray = resp.data;
			});
		},
	}
})


.factory('GetShopById', function($http,$rootScope,$localStorage) {
	var data = [];

	return {
		loadShopById: function(storeid,pushid){
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			
		/*	data1 = 
			{
				"categoryId" : $localStorage.custumerid 
			}				
			return  $http.post($rootScope.Host+'/getCategory.php',data1)
			.success(function(data, status, headers, config)
			{
					$rootScope.catArray = data;
					console.log("catagories: " , data)
					$scope.catArray = $rootScope.catArray;
					$scope.updateBasket();
					$rootScope.CatagoriesLoaded++;
			});		  */
			
			data1 = 
			{
				"storeId" : storeid,
				"pushid" : pushid
			}

			//alert (pushid);
			//console.log("D1 : " , params)
			return $http.post($rootScope.Host+'/getCustomerById.php',data1).then(function(resp)
			{
				console.log(" Resp : " , resp.data)
				$rootScope.getCustomerById = resp.data;
			});
		},
	}
})



.factory('CustomersDataService', function($q,$timeout,$rootScope) {

    var searchCustomers = function(searchFilter) {
         
       //console.log('Searching Customers for ' + searchFilter);

        var deferred = $q.defer();

	    var matches = $rootScope.Customers.filter( function(customer) {
	    	if(customer.name.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
	    })

        $timeout( function(){
        
           deferred.resolve( matches );

        }, 100);

        return deferred.promise;

    };

    return {

        searchCustomers : searchCustomers

    }
})

