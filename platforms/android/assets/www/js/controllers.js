angular.module('starter.controllers', [])


.service('BasketService', function() {
  
basket = 5;

this.returnBasket = function(){
	return basket;
}
  
})


.controller('AppCtrl', function($scope, $ionicModal, $timeout,$ionicSideMenuDelegate,$localStorage,$state,$rootScope,$ionicHistory,$ionicPopup) {

  $scope.$on('$ionicView.enter', function(e) {
 
  $scope.userTypeArray = ['לקוח רגיל','שליח','ספק','מנהל','לקוח עיסקי'];
  $scope.userStatusId =  $localStorage.manager;
  $scope.userStatus = $scope.userTypeArray[$scope.userStatusId];
  $scope.loggedinFullName = $localStorage.name;
  $scope.email = $localStorage.email;
  $scope.UserId = $localStorage.userid;
  
  
	$scope.changeCatagory = function(cat,index)
	{
		$ionicHistory.nextViewOptions({
			disableAnimate: true,
			expire: 300
		});
		window.location.href = $rootScope.productPage+index;
	}
	
	
	$scope.Reset =  function()
	{
			$rootScope.basketId = '';
			$scope.BasketPrice = 0;
			$rootScope.BasketArray = [];
			$localStorage.BasketArray = '';
			window.location.href = "#/app/main";
	}
	
	$scope.showCatagories= function()
	{
		$ionicSideMenuDelegate.toggleRight();
	}
	
  $scope.LogOut = function()
  {
	  if($rootScope.BasketArray.length  != 0)
		{  
			
			   var confirmPopup = $ionicPopup.confirm({
				 title: 'מחיקת סל קניות',
				 template: 'חזרה לתפריט הראשי תמחק את כל הפריטים בסל הקניות שלך'
			   });
			
			   confirmPopup.then(function(res) {
				 if(res) 
				 {
				    $rootScope.BasketArray = [];
				  	$localStorage.userid = '';
					$localStorage.custumerid = '';
					$localStorage.agentid = '';
					$localStorage.name = '';
					$localStorage.email = '';
					$localStorage.phone = '';
					$localStorage.address = '';
					$localStorage.manager = '';
					$localStorage.usertype = '';
					$localStorage.custumerimage = '';
					$localStorage.custumername = '';
					$localStorage.discount = '';
					$rootScope.CatagoriesLoaded = 0;
					$rootScope.selectedStore = "";
					$rootScope.storeName = "";
					$rootScope.loadOnce = 0;
					$localStorage.custumerDetails = '';
					$localStorage.BasketArray = '';
					$state.go('app.intro');	
	
				 } else {
				   console.log('You are not sure');
				 }
			});
			
		}
		else
		{
			window.location.href="#/app/intro"	
		}	
		
	
  }
  
  
  $scope.backToMenu = function()
  {		 
  		if($rootScope.BasketArray.length  != 0)
		{  
			
			   var confirmPopup = $ionicPopup.confirm({
				 title: 'מחיקת סל קניות',
				 template: 'חזרה לתפריט הראשי תמחק את כל הפריטים בסל הקניות שלך'
			   });
			
			   confirmPopup.then(function(res) {
				 if(res) 
				 {
				   $localStorage.BasketArray = '';
				   $rootScope.BasketArray = [];
				   window.location.href="#/app/intro"
				 } else {
				   console.log('You are not sure');
				 }
			});
			
		}
		else
		{
			window.location.href="#/app/intro"	
		}	
  }
  
  
 });

 
})


.controller('IntroCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicSideMenuDelegate,GetShopById,$ionicLoading,$q,CustomersDataService,UpdateShopByUser) 
{
	$scope.showCustomers = 0;
	
	if($localStorage.BasketArray  && $localStorage.BasketArray != '' )
	{
		console.log("LocalIn : " , $localStorage.BasketArray)
		$rootScope.BasketArray = $localStorage.BasketArray;
	}
	
	if ($localStorage.custumerid)
	{
		$state.go('app.main');	 
	}
	
	$scope.store = 
	{
		"id" : ""
	}
	
	$scope.data = { "customers" : [], "search" : '' };
	
	$http.get($rootScope.Host+'/getCustomers.php').success(function(data)
	{
		$rootScope.Customers = data;	
		console.log("Customers ", data)
		//$scope.CallCustomers();
	});	
	 
	$scope.CallCustomers = function()
	{
		CustomersDataService.searchCustomers($scope.data.search).then(
    		function(matches) {
				
    			$scope.data.customers = matches;
    		}
    	)
	}
	
	$scope.search = function() {
		
		if($scope.data.search != "" && $scope.data.search.length > 1)
		{
			$scope.store.id = "";
			$scope.showCustomers = 1;
			CustomersDataService.searchCustomers($scope.data.search).then(
				function(matches) {
					$scope.data.customers = matches;
				}
			)
		}
		else
		{
			$scope.showCustomers = 0;
		}
		
    }
		
	$scope.itemClicked = function(Name,Index)
	{
		//console.log("Index : " ,Index )
		$scope.store.id = Index;
		$scope.showCustomers = 0;
		$scope.data.search = Name;
	}
	
	$scope.getDetails = function()
	{
		console.log(" STID " , $scope.store.id)
		if($scope.store.id)
		{
			GetShopById.loadShopById($scope.store.id,$rootScope.pushId).then(function(data)
			{
				console.log("Data11 : " , $rootScope.getCustomerById);
				if ($rootScope.getCustomerById.response.status == 0)
				{
					$ionicPopup.alert({
					 title: 'קוד חנות שגוי יש לנסות שוב',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });	
					
					$scope.store.id = '';
				}
				else
				{
						
					UpdateShopByUser.UpdateShop($scope.store.id,$localStorage.userid,$rootScope.pushId)
					{
						
					}
					
					console.log(" Resp1 : ")
					//$localStorage.userid = '';
					$localStorage.custumerid = $rootScope.getCustomerById.response.custumerid;
					$localStorage.showmainpage	= $rootScope.getCustomerById.response.showmainpage;
					$localStorage.custumerimage = $rootScope.getCustomerById.response.custumerimage;
					$localStorage.custumername = $rootScope.getCustomerById.response.custumername;	
					$localStorage.manager = "0";
					$localStorage.usertype = "0";	
					$scope.store.id = '';
					$scope.data.search = "";
					$state.go('app.main');	 
				}
	
	
		
				//alert ($rootScope.getCustomerById);
				
				/*if(ShowMain == 0)
					$state.go('app.main');
				else
					window.location.href = "#/app/products_template2/0"
					
				if($localStorage.BasketArray  && $localStorage.BasketArray != '' )
				{
					$rootScope.BasketArray = $localStorage.BasketArray;
				}*/
			});	
		}
		else
		{
			alert("חובה לבחור חנות מהרשימה")
		}
	}
	
	$scope.exampleStore = function()
	{
		console.log(" STID11 " )
		GetShopById.loadShopById('4',$rootScope.pushId).then(function(data)
		{
			console.log("Data11 : " , data);
			
			
			console.log("Data11 : " , $rootScope.getCustomerById);

				//$localStorage.userid = '';
				$localStorage.custumerid= $rootScope.getCustomerById.response.custumerid;
				$localStorage.showmainpage	= $rootScope.getCustomerById.response.showmainpage;
				$localStorage.custumerimage = $rootScope.getCustomerById.response.custumerimage;
				$localStorage.custumername = $rootScope.getCustomerById.response.custumername;	
				$localStorage.manager = "0";
				$localStorage.usertype = "0";	
				$scope.store.id = '';
				$state.go('app.main');	 
		});		
	}
	
})

.controller('LoginRegisterCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicSideMenuDelegate,GetCustomersFactory,$ionicLoading,$q) {

	//if (!$localStorage.custumerid)
	//$localStorage.userid = '';


	$ionicSideMenuDelegate.canDragContent(false);
	
	$scope.getDetails = function(ShowMain)
	{
		GetCustomersFactory.loadCustomers().then(function(data)
		{
			if(ShowMain == 0)
				$state.go('app.main');
			else
				window.location.href = "#/app/products_template2/0"
				
			if($localStorage.BasketArray  && $localStorage.BasketArray != '' )
			{
				console.log("LocalIn")
				$rootScope.BasketArray = $localStorage.BasketArray;
			}
		});		
	}
	
	/*
	if ($localStorage.userid)
	{
		if($localStorage.BasketArray  && $localStorage.BasketArray != '' )
		{
			$rootScope.BasketArray = $localStorage.BasketArray;
		}
			
		if($localStorage.showmainpage == 0)
		$scope.getDetails(0);
		else
		$scope.getDetails(1);
		//$state.go('app.main');
	}
	*/
	
	
	$scope.login = 
	{
		"email" : "",
		"password" : ""
	}
	
	$scope.forgot = 
	{
		"email" : ""
	}
	
	$scope.register = 
	{
		"name" : "",
		"address" : "",
		"phone" : "",
		"email" : "",
		"username" : "",
		"password" : "",
		"code" : ""
	}
	
	
	
	$scope.LoginBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailregex = /\S+@\S+\.\S+/;

		if ($scope.login.email =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא שם משתמש',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.login.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא סיסמה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
		}

		else
		{
			login_data = 
			{
				"cid" : $localStorage.custumerid,
				"email" : $scope.login.email,
				"password" : $scope.login.password,
				"pushid" : $rootScope.pushId
			}					
			$http.post($rootScope.Host+'/login.php',login_data).success(function(data)
			{
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: 'כתובת מייל או סיסמה שגוים יש לתקן',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });					
				}
				else
				{	
					$localStorage.userid = data.response.userid;
					//$localStorage.custumerid = data.response.custumerid;
					$localStorage.agentid = data.response.agentId;
				//	$localStorage.custumername = data.response.custumername;
					$localStorage.name = data.response.name;
					$localStorage.email = data.response.email;
					$localStorage.phone = data.response.phone;
					$localStorage.address = data.response.address;
					$localStorage.manager = data.response.manager;
					$localStorage.usertype = data.response.usertype;
					$localStorage.showmainpage = data.response.showmainpage;
					//$localStorage.custumerimage = data.response.custumerimage;
					$localStorage.discount = data.response.discount;
					
					console.log("UID " , $rootScope.Host+'/get_info.php?id='+$localStorage.custumerid)
				/*	$http.get($rootScope.Host+'/get_info.php?id='+$localStorage.custumerid).success(function(data)
					{
						console.log("UID1 " , data)
						$localStorage.custumerDetails = data;	
						//console.log('SettingsArray: ',$rootScope.SettingsArray);
					});	*/
					
					
					//alert ($localStorage.discount)
					console.log("STT12 : " + data.response.showmainpage)
					
			
					if($localStorage.BasketArray  && $localStorage.BasketArray != '' )
					$rootScope.BasketArray = $localStorage.BasketArray;
					
					window.location.href = "#/app/basket";
					/*
					if (data.response.showmainpage == 0)
					$scope.getDetails(0);
					else
					$scope.getDetails(1);
					//window.location.href = $rootScope.productPage+0;	
					*/
					$scope.login.email = '';
					$scope.login.password = '';					
				}
			});			
		}
	}
	
	$scope.sendPassBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailregex = /\S+@\S+\.\S+/;
		
		if ($scope.forgot.email =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מייל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else if (!emailregex.test($scope.forgot.email))
		{
			$ionicPopup.alert({
			 title: 'מייל לא תקין יש לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else
		{
			forgot_data = 
			{
				"email" : $scope.forgot.email,
				"send" :  1
			}					
			$http.post($rootScope.Host+'/forgot_pass.php',forgot_data).success(function(data)
			{
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: 'מייל לא נמצא במערכת נא לנסות שוב',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });		
				}
				else
				{
					$ionicPopup.alert({
					 title: 'סיסמה נשלחה בהצלחה למייל שהזנת',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });		

					$state.go('app.intro');				   
				}

				$scope.forgot.email = '';
				
				
			});	
		}
	}
	
	$scope.RegisterBtn = function()
	{
		
			console.log("Register " , $localStorage.custumerid)	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailregex = /\S+@\S+\.\S+/;

		if ($scope.register.name =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא שם מלא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}

		else if ($scope.register.address =="" || $scope.register.address.formatted_address =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מגורים',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		/*
		else if ($scope.register.phone =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא מספר טלפון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		*/

		
		else if ($scope.register.email =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מייל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else if (!emailregex.test($scope.register.email))
		{
			$ionicPopup.alert({
			 title: 'מייל לא תקין יש לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });	

			$scope.register.email = '';
		}
		

		/*
		else if ($scope.register.username =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שם משתמש',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		*/
		
		else if ($scope.register.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא סיסמה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		/*else if ($scope.register.code =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא קוד חנות',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}*/		
		else
		{
			
			register_data = 
			{
				"name" : $scope.register.name,
				"address" : $scope.register.address.formatted_address,
				"phone" : $scope.register.phone,
				"email" : $scope.register.email,
				//"username" : $scope.register.username,
				"password" : $scope.register.password,
				"code" : $scope.register.code,
				"custumerid" : $localStorage.custumerid,
				"pushid" : $rootScope.pushId
			}					
			$http.post($rootScope.Host+'/register_private.php',register_data).success(function(data)
			{
				
				if (data.response.status == 4)
				{
					$ionicPopup.alert({
					 title: 'מייל כבר בשימוש יש להזין כתובת מייל אחרת',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });
				   
				   $scope.register.email = '';
				}
			/*	else if (data.response.codefound == 0)
				{
					$ionicPopup.alert({
					 title: 'קוד חנות לא נמצא יש לנסות שוב',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });
				   
					$scope.register.code = '';
				}	*/
				else if (data.response.status == 3) // && data.response.codefound == 1
				{
					$localStorage.userid = data.response.userid;
				//	$localStorage.custumerid  = $localStorage.custumerid,//data.response.custumerid;
					$localStorage.name = $scope.register.name;
					$localStorage.email = $scope.register.email;
					$localStorage.phone = "";
					$localStorage.address = $scope.register.address.formatted_address;
					$localStorage.custumerimage = data.response.custumerimage;
					$localStorage.manager = "0";

					
					$localStorage.agentid = "0";
					$localStorage.usertype = "2";
					//$localStorage.custumername = data.response.custumername;
					$localStorage.showmainpage = data.response.showmainpage;
					$localStorage.discount = data.response.discount;
					//alert ($localStorage.discount)		
			
					window.location.href = "#/app/basket";
					/*
					if (data.response.showmainpage == 0)
					$scope.getDetails(0);
					else
					$scope.getDetails(1);
					*/
				}

				//return;
				/*
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: 'אימייל כבר בשימוש יש לבחר מייל אחר',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });	

					$scope.register.email = '';
				}
				else
				{

				}
				
				*/
				
				
				
			});
		}
	}
	
  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {


	
	$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email,profileInfo.gender);

	
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,gender,first_name,last_name,locale&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.FaceBookLoginBtn = function() {
	  
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);


					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {

							
						$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email,profileInfo.gender);
							


						//$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
				
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

				//console.log('getLoginStatus', success.status);
 
				$ionicLoading.show({
				  template: 'loading...<ion-spinner icon="spiral"></ion-spinner>'
				});
 

				// Ask the permissions you need. You can learn more about
				// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };


  $scope.FacebookLoginFunction = function(id,firstname,lastname,email,gender)
  {
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		$scope.fullname = firstname+' '+lastname;
		$scope.gender = (gender == "male" ? "זכר" : "נקבה");
			
			facebook_data = 
			{
				"id" : id,
				"firstname" : firstname,
				"lastname" : lastname,
				"email" : email,
				"fullname" : $scope.fullname,
				"gender" : $scope.gender,
				//"push_id" : $rootScope.pushId 
			}					
			$http.post($rootScope.Host+'/facebook_connect.php',facebook_data).success(function(data)
			{

			
					$localStorage.userid = data.response.userid;
					$localStorage.custumerid = data.response.custumerid;
					$localStorage.agentid = data.response.agentId;
					$localStorage.custumername = data.response.custumername;
					$localStorage.name = $scope.fullname;
					$localStorage.email = email;
					$localStorage.phone = data.response.phone;
					$localStorage.address = data.response.address;
					$localStorage.manager = data.response.manager;
					$localStorage.usertype = data.response.usertype;
					$localStorage.showmainpage = data.response.showmainpage;
					$localStorage.custumerimage = data.response.custumerimage;
					$localStorage.discount = data.response.discount;

					
				if (data.response.address == "")
				{
				
					window.location.href = "#/app/updateprofile"
				}
				else
				{
					window.location.href = "#/app/basket";
					/*
					if (data.response.showmainpage == 0)
					$scope.getDetails(0);
					else
					$scope.getDetails(1);					
					*/
				}
			});

  }  
  
  
  
	
})



.controller('UpdateProfileCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,BasketService,$ionicLoading) 
{
	$scope.fields = 
	{
		"address" : "",
		"code" : ""
	}
	$scope.saveDetails = function()
	{

		if ($scope.fields.address =="" || $scope.fields.address.formatted_address =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מגורים',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		
		else if ($scope.fields.code =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא קוד חנות',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}	

		else
		{
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				
			send_data = 
			{
				"user" : $localStorage.userid,
				"address" : $scope.fields.address.formatted_address,
				"code" : $scope.fields.code,
			}				
			$http.post($rootScope.Host+'/save_info.php',send_data)
			.success(function(data, status, headers, config)
			{
		
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: 'קוד חנות לא נמצא יש לנסות שוב',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });
				   
					$scope.fields.code = '';					
				}
				else
				{
					//$localStorage.userid = data.response.userid;
					$localStorage.custumerid = data.response.custumerid;
					$localStorage.custumerimage = data.response.custumerimage;
					$localStorage.custumername = data.response.custumername;
					
					/*
					$localStorage.agentid = data.response.agentId;
					$localStorage.name = $scope.fullname;
					$localStorage.email = email;
					$localStorage.phone = data.response.phone;
					$localStorage.address = data.response.address;
					$localStorage.manager = data.response.manager;
					$localStorage.usertype = data.response.usertype;
					$localStorage.showmainpage = data.response.showmainpage;
					$localStorage.discount = data.response.discount;
					*/
					$state.go('app.main');
				}
				
				
				
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});
	
	
		}
		
		
		
		
	}
})


.controller('MainCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,BasketService,$ionicLoading) 
{
	//$localStorage.userid = '';
	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host; 
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$rootScope.ProductsArray = [];
	
	$ionicLoading.show({
      template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
    });

	if (!$localStorage.custumerid)
	{
		console.log("GoBack")
		//$state.go('app.intro');	 
	}	
					
	  $scope.$on('imageloaded', function(events, args){
		$ionicLoading.hide();
	  })
	
	 //if ($rootScope.CatagoriesLoaded == 0)
	  $rootScope.getCatagories();
	 
	  if ($localStorage.showmainpage != 0 && $localStorage.custumerid)
	  {
	  	window.location.href = "#/app/products_template2/0"
	  }
  
  
/*  $scope.getCatagories = function()
  {
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	data1 = 
	{
		"categoryId" : $localStorage.custumerid 
	}				
	$http.post($rootScope.Host+'/getCategory.php',data1)
	.success(function(data, status, headers, config)
	{
		$ionicLoading.hide();
		$rootScope.catArray = data;
//		console.log("catagories: " , data)
		$scope.catArray = $rootScope.catArray;
		$scope.updateBasket();
		$rootScope.CatagoriesLoaded++;		

		
	})
	.error(function(data, status, headers, config)
	{
		$ionicLoading.hide();
	});	

	
  }*/
   
  if ($rootScope.CatagoriesLoaded == 0)
  $scope.getCatagories();
		
  $rootScope.$watch('catArray', function(value) 
  {
	  $scope.catArray = $rootScope.catArray;
	  $scope.updateBasket();
	  
	  if ($localStorage.showmainpage != 0 && $localStorage.custumerid)
	  {
	  	window.location.href = "#/app/products_template2/0"
	  }
  
  });		
	 
	$scope.updateBasket = function()
	{
		//$scope.BasketPrice  = 0;
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}
	
	$scope.updateBasket();
	
	$scope.goProductPage = function(Cat,index)
	{
		window.location.href = $rootScope.productPage+index;
	}
		
})


.controller('ProductsCtrl', function($scope,$ionicScrollDelegate, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,BasketService,$ionicLoading,$ionicSideMenuDelegate,GetCustomersFactory,$timeout) {
	
	//Get Category 
	/*$http.get('http://appdev.co.il/laravel/public/data').success(function(data)
	{
		console.log("LocalHost Return12 : " , data)
	});*/
	//scrollTo(left, top, [shouldAnimate])
	//$rootScope.positionTop = 
	
	/*$scope.$on("$destroy", function() 
	{
    	var delegate = $ionicScrollDelegate.$getByHandle('start');
    	delegate. forgetScrollPosition();
	});
	
	$ionicScrollDelegate.$getByHandle('start').scrollTo($rootScope.positionTop);*/
	 
	$ionicSideMenuDelegate.canDragContent(false);
	$scope.userType = $localStorage.usertype;
	
	$timeout(function()
	{
		 $ionicScrollDelegate.scrollTo(0, $rootScope.positionTop, false);
		//window.scrollTo(0, 1200);
    },100);
	
/*	if (!$localStorage.custumerid)
	{
		$state.go('app.intro');	 
	}*/

	
	
	/*$timeout(function()
	{
		var startHandle = _.find($ionicScrollDelegate._instances, function (s) {
        return s.$$delegateHandle === "start";
    });
		startHandle.scrollTo($rootScope.positionTop);
		//$ionicScrollDelegate.$getByHandle('scroller').scrollTo($rootScope.positionTop);
	},1500)*/
	
	// $ionicScrollDelegate.scrollTo(900);
	$scope.iCounter = 0;
	$scope.getScrollPosition = function()
	{
   		/*console.log("Scroll : " , $ionicScrollDelegate.getScrollView())
		var maxScrollableDistanceFromTop = $ionicScrollDelegate.$getByHandle('scroller').getScrollView().__maxScrollTop;
		var maxScrollableDistanceFromTop1 = $ionicScrollDelegate.$getByHandle('scroller').getScrollView().__contentHeight;
		console.log(maxScrollableDistanceFromTop1)*/
		//alert("IN")
		$scope.iCounter++;
		
		
	}
	
	/*$ionicScrollDelegate.getScrollView().onScroll = function () 
	{
  		//console.log($ionicScrollDelegate.getScrollPosition());
		//$scope.iCounter++;
		//$scope.scrollTop = $ionicScrollDelegate.$getByHandle('scroller').getScrollPosition();
		//$scope.scrollTop = $scope.i; 
	};*/

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

	get_data = 
	{
		"custumerid" : '111',
		"agentid" : '222',
		"user" : '333'
	}

	
			
	
	$scope.gotoDetailsPage = function(product,index,category)
	{
		//ui-sref="app.details({Type: 'products', ItemId: item.index, CatagoryId: Category})"
		$rootScope.positionTop = $ionicScrollDelegate.$getByHandle('scroller').getScrollPosition().top;
		window.location.href = "#/app/details/"+product+"/"+index+"/"+category;
	}
	
   // $scope.$on('$ionicView.enter', function(e) {
	$scope.Category = $stateParams.ItemId;
	$scope.NextCategoryIndex = parseInt($stateParams.ItemId)+1;
	$scope.OldCategoryIndex = parseInt($scope.Category)-1;
	$scope.host = $rootScope.Host;
	$scope.CatagoryName = "";
	$scope.itemBasket = [];
	$scope.BasketPrice = 0;
	$scope.navTitle= '<p style="width:140px; margin-left:-15px; margin-top:5px;">'+$scope.CatagoryName +'</p>';
	$scope.catArray  = $rootScope.catArray;
	$scope.productsearch = '';
	//$scope.Catagories  = $rootScope.catArray[0].name;
	//alert ($scope.Catagories);
	$scope.ProductsArray = [];
//	console.log("products:  " , $rootScope.catArray);
	$scope.imagesArray = [];
	$scope.currentArray = [];
	$scope.AllProducts = [];
	$scope.checkExists = 1;
	
	$scope.arrayPlace = "";
	
	$ionicLoading.show({
      template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
    });
	
	
	$ionicSideMenuDelegate.canDragContent(false);
				
	$scope.$on('imageloaded', function(events, args){
		$ionicLoading.hide();
	})
	
	$rootScope.$ionicGoBack = function() {
		window.location.href = "#/app/main";
	}; 

	$scope.showCatagories= function()
	{
		$ionicSideMenuDelegate.toggleRight();
	}

	$scope.$watch('productsearch', function(value) 
	{
		
			if($scope.productsearch.length > 0)
				$scope.currentArray = $scope.AllProducts;
			else if($rootScope.catArray[$scope.Category])
				$scope.currentArray = $rootScope.catArray[$scope.Category].products;
		//console.log("Carray " ,$scope.currentArray )
	});
	 
	
	$scope.changePosition = function(type)
	{
		$rootScope.positionTop = 0;
		
		$timeout(function()
		{
			  $ionicScrollDelegate.scrollTo(0, $rootScope.positionTop, false);
			//window.scrollTo(0, 1200);
		},300);
		 $ionicScrollDelegate.scrollTo(0, 0, false);
		$scope.newCat = $scope.catArray[$scope.NextCategoryIndex];
		$scope.previousIndex = parseInt($scope.NextCategoryIndex)-1;
		$scope.oldCat = parseInt($scope.NextCategoryIndex)-1;
		$scope.oldIndex = $scope.catArray[$scope.oldCat];
		
		
		if (type == 1)
		{
			if ($scope.catArray.length > $scope.NextCategoryIndex  )
			{
				$scope.changeCatagory($scope.newCat.index,$scope.NextCategoryIndex);
			}

			
		}
		else
		{
			if ($scope.oldCat > 0)
			{
				$scope.changeCatagory($scope.oldIndex.index,$scope.OldCategoryIndex);
			}
		}
		
		
		
		//  $ionicSideMenuDelegate.canDragContent(true);
		//$scope.CatIndex = $scope.ProductsArray['index'];
		//alert ($scope.CatIndex)
		
		
		//alert (123);
		//alert (type);
	}

	$scope.loadProducts = function(type)
	{
		if (type == 1)
			$rootScope.loadOnce = 0;
		
		$scope.AllProducts = [];
		$scope.ProductsArray = $rootScope.catArray[$scope.Category];
		
		for(i=0;i<$rootScope.catArray.length;i++)
		{
			var PR = $rootScope.catArray[i].products;
			for(j=0;j<PR.length;j++)
			{
				if (!PR[j].quan)
					PR[j].quan = PR[j].minorder;
				if (!PR[j].remarks)	
					PR[j].remarks = "";
						
				$scope.AllProducts.push(PR[j])
			}
		}
		
		$rootScope.AllProducts = $scope.AllProducts;
		if($scope.ProductsArray)
		{
			if($scope.ProductsArray.products.length == 0)
			$ionicLoading.hide();
		}else
		{
		//	window.location.href = "#/app/intro";
			//return;
		}
		
		
	
		
		if($scope.ProductsArray)
		{
			$scope.CatagoryName = $scope.ProductsArray['name'];
//			console.log("$scope.CatagoryName" , $scope.CatagoryName)	
			$scope.navTitle= '<p style="width:240px !important; margin-left:-15px; margin-top:5px; background-color:red">'+$scope.CatagoryName +'</p>';
		}
		
		
		
//		console.log("$rootScope.catArray",$scope.ProductsArray)
		
		
				for(var i=0;i< $scope.ProductsArray.products.length;i++)
				{
					//$scope.ProductsArray[i].isClicked = false;
					if ($scope.ProductsArray.products[i].quan > 0)
					{						
						$scope.ProductsArray.products[i].quan = $scope.ProductsArray.products[i].quan;
					}
					else
					{
						$scope.ProductsArray.products[i].quan = $scope.ProductsArray.products[i].minorder;
					}
						
					if (!$scope.ProductsArray.products[i].remarks)	
						$scope.ProductsArray.products[i].remarks = "";						

				
					if ($scope.ProductsArray.products[i].color)
					{
						for(var j=0;j< $scope.ProductsArray.products[i].color.length;j++)
						{
							
							if (!$scope.ProductsArray.products[i].color[j].selected)
							$scope.ProductsArray.products[i].color[j].selected = "0";
							
						}
					}

					if ($scope.ProductsArray.products[i].size)
					{
						for(var g=0;g< $scope.ProductsArray.products[i].size.length;g++)
						{
							
							if (!$scope.ProductsArray.products[i].size[g].selected)
							$scope.ProductsArray.products[i].size[g].selected = "0";
							
						}
					}
					
					
					
				}
				
//				console.log("product array : " , $scope.ProductsArray)
				
		//	}
			
			$scope.currentArray = $scope.ProductsArray.products;
			$rootScope.currentArray = $scope.currentArray;
			
//			console.log("$scope.currentArray", $scope.currentArray)
	//	}					
//			console.log("CT",$rootScope.catArray)
		
		
	

		$rootScope.loadOnce++;
		//alert ($rootScope.loadOnce);
	}

	$rootScope.$watch('catArray', function(value) 
	{
		$scope.catArray = $rootScope.catArray;
		
		if($scope.catArray.length > 0)
		$scope.loadProducts();
	});

	//$scope.catArray = $rootScope.catArray;
	//$scope.loadProducts();
	//$scope.catArray = $rootScope.catArray;
	//$scope.loadProducts();
	
	//if($rootScope.catArray.length == 0)
	//{ 
		/*InTest
		GetCustomersFactory.loadCustomers().then(function(data)
		{
			$scope.catArray = $rootScope.catArray;
	//		console.log("loadCustomers" , $rootScope.catArray[$scope.Category]);
			$scope.loadProducts();
		});		*/
	//}
	
	
	//$scope.loadProducts();

	
	$scope.changeCheckBox = function(index,item,state)
	{
		if (state == 1)
		{
			item.isClicked = 0;
			item.quan = "1";
		}		
		else
		{
			item.isClicked = 1;
			//$scope.updateBasket();
		}
		console.log("basket new : ", $rootScope.BasketArray)	
		
		$scope.AddBasket(index,item);
		
	}

	$scope.goProductPage = function(Cat,index)
	{
		
		//$rootScope.ProductsArray  = [];
		//$scope.ProductsArray = [];
		console.log("Cat" , index)
		$scope.Category = index;
		$scope.ProductsArray = $rootScope.catArray[$scope.Category];
		$scope.currentArray = $scope.ProductsArray.products;
		$rootScope.currentArray = $scope.currentArray;
		//$scope.loadProducts(1);	
		//alert ($scope.Category);
		//if ($rootScope.loadOnce == 0)
		
		//window.location.href = $rootScope.productPage+Cat;
	}
	
	$scope.AddBasket = function(index,item)
	{
		console.log("Basket" , index ,  item)
		//$scope.itemBasket = [];
		$scope.BasketPrice = 0;
		
		if ($rootScope.BasketArray.length == 0)
		$rootScope.BasketArray = new Array();
		
		
		if (item.isClicked == 1)
		$rootScope.BasketArray.push(item);
		else
		{
			item.isClicked = 0;
			for(var i=0;i< $scope.BasketArray.length;i++)
			{
				if ($scope.BasketArray[i].index == item.index)
				{
					$scope.BasketArray.splice(i, 1);
				}
			}
			
		}
		
		
/*
		for(var i=0;i< $scope.ProductsArray.length;i++)
		{
		
			if ($scope.ProductsArray[i].isClicked == 1)
			{

				console.log("product: ", $scope.ProductsArray[i])
				$rootScope.BasketArray.push($scope.ProductsArray[i]);
				$scope.ProductsArray[i].isClicked = 1;
			}			
			else
			{
				$scope.ProductsArray[i].isClicked = 0;
			}
		}
*/
		console.log("array1 new: ", $rootScope.BasketArray);
		
		//$scope.updateBasket();

		$scope.updateBasket();
/*
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
		   $scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		   console.log($rootScope.BasketArray[i].new_price+ ' : ' + $scope.BasketPrice)
		}		
*/
		
		console.log("scope basket array: ", $rootScope.BasketArray)
		console.log("------------------------------");

	}



	$scope.updateBasket = function()
	{
		
		$scope.BasketPrice  = 0;
		
		//console.log("BasketArray: " , $rootScope.BasketArray,$rootScope.BasketArray.length)
		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
//				console.log("basket price: ",$rootScope.BasketArray[i].new_price)
		}
		
	}

	$scope.updateBasket();

	

	
	
	
	$scope.CartAdd = function()
	{
		//$rootScope.BasketArray = $scope.itemBasket;
		console.log($rootScope.BasketArray);
		//$scope.CartOptions();
		window.location.href = "#/app/orderform";
		
		/*
			$ionicPopup.alert({
			 title: 'חבילה נוספה לסל בהצלחה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		 */
	}
	
	$scope.goToBasket = function()
	{
		console.log('basket: ', $scope.itemBasket);
		
		if ($scope.BasketPrice == 0)
		{
			$ionicPopup.alert({
			 title: 'יש תחילה להוסיף מוצרים לסל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });	
		}
		else
		{
			window.location.href = "#/app/basket";
		}
	}

	
	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}


	$scope.changeQuan = function(type,item,arrayindex)
	{
		$scope.checkExists = 1;
		
		console.log("basket: ",$rootScope.BasketArray);
		

			for(var i=0;i< $rootScope.BasketArray.length;i++)
			{
				if ($rootScope.BasketArray[i].index == item.index)
				{
					$scope.checkExists = 0;
					$scope.arrayPlace = i;
					
				}
				else
				{
					//$scope.checkExists = 1;
					//$scope.arrayPlace = i;
				}
				
			}			

		//alert ($scope.checkExists);

			
		if (type == 1)
		{
			item.quan++;
			
			if ($scope.checkExists == 0)
			{
				//alert ("exists");
			}
			else
			{
				$rootScope.BasketArray.push(item);
			}
			
			$scope.updateBasket();
			
		}
		else
		{
			
			if (item.quan > item.minorder)
			{
				item.quan = String(parseInt(item.quan)-1);
			}
			else 
			{
				$scope.minorder = parseInt(item.minorder)+1;
				
				$ionicPopup.alert({
				 title: 'לא ניתן להזמין פחות ממינימום הזמנה '+$scope.minorder+' פריטים',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });	
			}
			
			
			if (item.quan == 0 && $scope.checkExists == 0)
			{
				//alert ($scope.arrayPlace);
				$scope.BasketArray.splice($scope.arrayPlace, 1);

			}

			$scope.updateBasket();

		}
	}


//});
	
	
})



.controller('BasketCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

//	console.log('BasketCtrl' ,$rootScope.BasketArray )
	$scope.host = $rootScope.Host;
	$scope.BasketPrice = 0;
	$scope.BasketArray = $rootScope.BasketArray;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.canCheckOut = 0;
//	console.log('BasketCtrl2' ,$scope.BasketArray)
	$scope.discount = $localStorage.discount;
	$scope.timeStamp = Date.now(); 


		

	$scope.fields = 
	{
		"pickup" : "",
		"dest" : $localStorage.address,
		"deliverytype" : "",
		"remarks" : "",
		"deliverydate" : $scope.prettyDate,
		"store" : ""
	}


	$scope.stores = [];

	for(var i=0;i< $scope.BasketArray.length;i++)
	{
			$scope.BasketArray[i].isClicked = "1";
	}
	
		
	$scope.userType = $localStorage.usertype;
	
/*	
	$scope.getStores = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		get_data = 
		{
			"custumerid" : $localStorage.custumerid,
			"agentid" : $localStorage.agentid,
			"user" : $localStorage.userid
		}

		$http.post($rootScope.Host+'/get_user_stores.php',get_data).success(function(data)
		{
			$scope.stores = data;
			
			if ($scope.stores.length ==  0)
			{
				$scope.fields.store = $localStorage.custumerid;
				$scope.canCheckOut  = 1;
			}
			
			console.log("stores: " , $scope.stores)
		});			
	}
*/
	if ($scope.userType == 1)
	{
		//$scope.getStores();
	}

					
		
	$scope.checkoutBtn = function()
	{
		window.location.href = "#/app/orderform";	
	}
		

	$scope.deleteProduct = function(index)
	{
		$scope.BasketArray[index].isClicked = false;
		$scope.BasketArray.splice(index, 1);
		$scope.updateBasket();
		//console.log($scope.ProductsArray);
	}


	$scope.changeQuan = function(type,index,item)
	{
		if (type == 1)
		{
			$scope.BasketArray[index].quan = String(parseInt($scope.BasketArray[index].quan)+1);
		}
		else
		{
			
			if ($scope.BasketArray[index].quan > $scope.BasketArray[index].minorder)
			{
				$scope.BasketArray[index].quan = String(parseInt($scope.BasketArray[index].quan)-1);
			}
			else
			{
				$ionicPopup.alert({
				 title: 'לא ניתן להזמין פחות ממינימום הזמנה '+$scope.BasketArray[index].minorder+' פריטים',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });					
			}
				
		}
		
		$scope.updateBasket();
		
		
		
	}

	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}	

	
	$scope.updateBasket = function()
	{
		$scope.BasketPrice = 0;
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}

	$scope.updateBasket();
	
	$scope.changeCheckBox = function(index,item,type)
	{
		var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקת מוצר מהעגלה?'

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$scope.BasketArray[index].quan = "1";
			$scope.BasketArray[index].isClicked = "0";
			$scope.BasketArray.splice(index, 1);
			$scope.updateBasket();
		 } 
	   });
	   
	}
	
	$scope.AddBasket = function(index,item)
	{
		$scope.BasketArray[index].quan = item.quan;
		$scope.updateBasket();

	}

	$scope.checkExtras= function(item)
	{
		if (item)
		{
			$scope.CountSelected = 0;
			for(var i=0;i< item.length;i++)
			{
				//if (item[i].isClicked == 1)
					if (item[i].selected == 1)
					$scope.CountSelected++;
			}	
			
			if ($scope.CountSelected > 0)
				return true;
			else
				return false;			
		}

	}

	$scope.checkLoggedIn = function()
	{
			if ($rootScope.BasketArray.length == 0)
			{
				$ionicPopup.alert({
				 title: 'סל קניות ריק'  ,
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });				
			}
			else
			{
				if ($localStorage.userid)
				{
					$scope.completeOrder();
				}	
				else
				{
				   var confirmPopup = $ionicPopup.confirm({
					 title: 'יש להתחבר בכדי לבצע הזמנה',
					 template: 'האם ברצונך להתחבר/להרשם?'
				   });

				   confirmPopup.then(function(res) {
					 if(res) {
					   window.location.href = "#/app/login";
					 } else {
					   //return;
					 }
				   });					
				}	

			}
	}
    
	
	$scope.completeOrder = function()
	{
	
				console.log("userType ",$scope.userType )
				if ($scope.userType == 1)
				{
					$scope.storeId = $rootScope.selectedStore;
					$scope.userId = $rootScope.selectedStore;
					//$scope.userId =  $localStorage.userid;
					console.log("shop : " , $scope.userId)
					//
					if ($rootScope.selectedStore =="")
					{
						//alert (444);
						$ionicPopup.alert({
						 title: 'יש לבחור חנות'  ,
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });		

						$state.go('app.selectstore');
					}
					else
					{
						$scope.canCheckOut = 1;
					}
					
				}				
				else
				{
					$scope.storeId = $localStorage.custumerid;
					$scope.userId = $localStorage.userid;
					$scope.canCheckOut = 1;
				}
				
				if ($scope.canCheckOut == 1)
				{
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

//					console.log("basket array ", $rootScope.BasketArray)
					//return;
					if ($rootScope.basketId =="")
						$rootScope.basketId = $scope.timeStamp;
					
						//alert ($rootScope.basketId)
					
						//alert(JSON.stringify($rootScope.BasketArray));
						checkout_data = 
						{
							
							"custumerid" : $localStorage.custumerid,
							"agentid" : $localStorage.agentid,
							//"user" : $localStorage.userid,
							"user" : $scope.userId,
							"pickup" : $scope.fields.pickup,
							"dest" : $scope.fields.dest,
							
							"deliverytype" : $scope.fields.deliverytype,
							"remarks" : $scope.fields.remarks,
							//"deliverydate" : $scope.fields.deliverydate,
							"sum" : $scope.BasketPrice,
							"products" : $rootScope.BasketArray,
							"cartId" : $rootScope.basketId,
						}
						
						//alert (777);
						
						console.log("cartParam : ",checkout_data);
						//return;
						$http.post($rootScope.Host+'/checkout_cart.php',checkout_data).success(function(data)
						{
							//alert (555);
							console.log("cart1 : ",data);
								$rootScope.basketId = '';
								$scope.BasketPrice = 0;
								$scope.payAmount = 0;
								$scope.clearSelected();
								$rootScope.BasketArray = [];
								//$rootScope.BasketArray = new Array();
								$scope.fields.pickup = '';
								$scope.fields.dest = '';
								$scope.fields.deliverytype = '';
								$scope.fields.remarks = '';
								$scope.fields.deliverydate = '';

								$ionicPopup.alert({
								 title: 'תודה שהזמנת '+$localStorage.custumername+ ' ההזמנה נשלחה , נציג שירות יחזור אליך בהקדם'  ,
								buttons: [{
									text: 'אשר',
									type: 'button-positive',
								  }]
							   });	
							   
							   $localStorage.BasketArray = '';	
							   $rootScope.selectedStore = "";
							   $scope.storeName = "";
							   $scope.updateBasket();
							   window.location.href = "#/app/main";
							   $rootScope.selectedStore == "";
							   $scope.storeId = $rootScope.selectedStore;
							   $scope.canCheckOut = 0;
							   $rootScope.storeName = "";
						});						
				}			
			
	}
	
	$scope.clearSelected = function()
	{
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$rootScope.BasketArray[i].isClicked = false;
				$rootScope.BasketArray[i].quan = "0";
		}			
	}	
	
	$scope.showDetails = function()
	{
		console.log("basket: ",$scope.BasketArray)
		
	}

	if ($scope.discount)
	{
		$scope.priceAfterDiscount = $scope.BasketPrice-(($scope.discount*$scope.BasketPrice)/100);
		//alert ($scope.priceAfterDiscount)
	}
	else
	{
		$scope.priceAfterDiscount  = $scope.BasketPrice;
	}	
})

.controller('ShopsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.host = $rootScope.Host;
	$scope.BasketPrice = 0;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';

	$scope.StoresArray = $rootScope.storesArray;
	
	

	$scope.updateBasket = function()
	{
		//$scope.BasketPrice  = 0;
		
		console.log("basket array: ",$rootScope.BasketArray)
		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}

	$scope.updateBasket();	
	

})

.controller('StoreMenuCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice  = 0;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';

	$scope.host = $rootScope.Host;
	$scope.MenuId = $stateParams.ItemId;
	$scope.menuArray = $rootScope.storesArray[$scope.MenuId].products;
	console.log("menu: ",$scope.menuArray)

	
	$scope.checkExtras= function(item)
	{
		if (item)
		{
			$scope.CountSelected = 0;
			for(var i=0;i< item.length;i++)
			{
				if (item[i].isClicked == 1)
					$scope.CountSelected++;
			}	
			
			if ($scope.CountSelected > 0)
				return true;
			else
				return false;			
		}

	}
	
	for(var i=0;i< $scope.menuArray.length;i++)
	{
		$scope.menuArray[i].quan = "1";
		$scope.menuArray[i].isClicked = 0;
		$scope.menuFlag = 0;
		
		for(var j=0;j< $rootScope.BasketArray.length;j++)
		{	
			if ($rootScope.BasketArray[j].index == $scope.menuArray[i].index)
			{
				$scope.menuFlag = 1;
			}
		}
		
			if ($scope.menuFlag)
			{
				$scope.menuArray[i].isClicked = 1;
			}
		
		/*
			if ($scope.menuArray[i].isClicked == 1)
				$scope.menuArray[i].isClicked = 1;
			else
				$scope.menuArray[i].isClicked = 0;
		*/
	}	

		
		
	$scope.changeCheckBox = function(index,item,state)
	{
		if (state == 1)
		{
			item.isClicked = 0;
			item.quan = "1";
		}		
		else
		{
			item.isClicked = 1;
			$state.go('app.extras', { StoreId: $scope.MenuId, ProductId: index });

			//window.location.href = "#/app/extras/"+$scope.MenuId+"/"+$scope.menuArray;
		}
			
		$scope.AddBasket(index,item);
	}



	$scope.AddBasket = function(index,item)
	{
		
		//$scope.itemBasket = [];
		$scope.BasketPrice = 0;
		//$rootScope.BasketArray = new Array();
		
		for(var i=0;i< $scope.menuArray.length;i++)
		{
		
			if ($scope.menuArray[i].isClicked == 1)
			{
				$rootScope.BasketArray.push($scope.menuArray[i]);
				$scope.menuArray[i].isClicked = 1;
			}			
			else
			{
				$scope.menuArray[i].isClicked = 0;
			}
		}

		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
		   $scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		   console.log($rootScope.BasketArray[i].new_price+ ' : ' + $scope.BasketPrice)
		}		
		
		//$rootScope.BasketArray = new Array();
		//$rootScope.BasketArray = $scope.itemBasket;
		//console.log("basket array: ", $rootScope.BasketArray);
		
		console.log("scope basket array: ", $rootScope.BasketArray)
		console.log("------------------------------");

		//console.log(item);
		//alert (444);
	}

	
	
	$scope.updateBasket = function()
	{	
		//console.log("basket array: ",$rootScope.BasketArray)
		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
		
		
	}

	$scope.updateBasket();

	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}

	
})


.controller('ExtrasCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.host = $rootScope.Host;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';

	$scope.StoreId = $stateParams.StoreId;
	$scope.ProductId = $stateParams.ProductId;
	$scope.BasketPrice = 0;
	$scope.selectedExtras = [];


	$scope.extrasArray = $rootScope.storesArray[$scope.StoreId].products[$scope.ProductId].extras;
	$scope.ProductArray = $rootScope.storesArray[$scope.StoreId].products[$scope.ProductId];
	$scope.ProductId = $scope.ProductArray.index;	
	console.log("extras: " , $scope.extrasArray)
	
	if (!$scope.extrasArray[0].isClicked)
	{
		for(var i=0;i< $scope.extrasArray.length;i++)
		{
			$scope.extrasArray[i].isClicked = 0;
		}			
	}
	


	
	$scope.changeCheckBox = function(index,item,state)
	{
		if (state == 1)
		{
			item.isClicked = 0;
			//item.quan = "1";
		}		
		else
		{
			item.isClicked = 1;
		}
			
		//$scope.AddBasket(index,item);
	}

	$scope.saveExtras = function()
	{
		//console.log("saved extras : " , $scope.extrasArray);
		console.log("basket: ",$rootScope.BasketArray);

		/*
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
			if ($scope.extrasArray[i].isClicked == 1)
			{
		*/
		

		
	}


	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();

	

})



.controller('AboutCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$sce) {
	
		
		
	$http.get($rootScope.Host+'/get_info.php?id='+$localStorage.custumerid).success(function(data)
	{
		$localStorage.custumerDetails = data;	
		
		$scope.BasketPrice = 0;
		$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
		
		if($localStorage.custumerDetails[0].desc != "")
		$scope.AboutText = $localStorage.custumerDetails[0].desc;
		else
		$scope.AboutText = "אין מידע";
		$scope.Gallery = $localStorage.custumerDetails[0].gallery;
		$scope.Banners = $localStorage.custumerDetails[0].banners;
		$scope.Info = $localStorage.custumerDetails[0];
		$scope.host = $rootScope.Host;
	});	
					
					
	
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();
	
	
	$scope.dialPhone = function()
	{
		//alert ($scope.Sapak.phone)
		
		//window.location ="tel:"+$scope.Sapak.phone;
		window.location ="tel://"+$scope.Info.phone;
	}
	$scope.Waze = function()
	{
		//alert ($scope.Sapak.waze)
		window.location = "http://waze.to/?ll="+$scope.Info.waze+"&navigate=yes";
	}
	$scope.Video = function()
	{
		if ($scope.Info.video)
		{

			$scope.VideoLink = $sce.trustAsResourceUrl($scope.Info.video);

			$ionicModal.fromTemplateUrl('video-modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(VideoModal) {
			  $scope.VideoModal = VideoModal;
			  $scope.VideoModal.show();
			 });

			/*	
			$ionicModal.fromTemplateUrl('templates/video_modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(VideoModal) {
			  $scope.VideoModal = VideoModal;
			  $scope.VideoModal.show();
			 });			
			*/
		}
		else
		{
			$ionicPopup.alert({
				 title: 'לא נמצא סרטון יש לנסות מאוחר יותר',
				 template: ''
			   });			
		}
		

		 
		//window.location  = ;
	}
	
	$scope.stopVideo = function()
	{
		var div = document.getElementById('videoWrapper');
		var player = document.getElementById('VideoPlayer');
		div.innerHTML = '';
		div.remove();
		player.src='';

	}

			
	$scope.closeVideoModal= function()
	{
		alert("Close")
		$scope.VideoModal.hide();
		$scope.stopVideo();
	}
	
	
	$scope.facebbok = function()
	{
		window.location = $scope.Info.facebbok;
	}
	
	$scope.site = function()
	{
		window.location = $scope.Info.site;
	}
	

	
})


.controller('CouponCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$sce) 
{
	$scope.host = $rootScope.Host;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$http.get($rootScope.Host+'/get_coupons.php?id='+$localStorage.custumerid).success(function(data)
	{
		console.log("coupons : ", data)
		$scope.Coupons = data;	
		$rootScope.CouponsArray = $scope.Coupons;
	});	
	
	$scope.checkPercent = function(deal)
	{
		z=deal.old_price-deal.price;
		z= z/deal.old_price;
		z = parseInt(z*100);
		//alert(parseInt(deal.price)*parseInt(deal.discount_precent/10))
		return z;
	}	

	$scope.cutDate = function(date)
	{
		var dArray = date.split('/');
		dateStr = dArray[0]+"."+dArray[1]
		return dateStr;
	}
	
	$scope.cutYear = function(date)
	{
		var dArray = date.split('/');
		dateStr = dArray[2]
		return dateStr
	}	
})


.controller('CouponsDetailsCtrl',  function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$sce,$timeout) 
{
	$timeout(function() {
		$scope.host = $rootScope.Host;
		console.log("0" , $stateParams.ItemId)		
		$scope.Deal = $rootScope.CouponsArray[$stateParams.ItemId];
		console.log("1" , $scope.Deal)
		$scope.Gallery = [$scope.Deal.image,$scope.Deal.image2];
    }, 100);
})


.controller('SpecialRequestsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.OrderTime = 0;
	$scope.DeliveryPrice = 15;
	$scope.TotalPrice = 0;
	
	$scope.fields = 
	{
		"pickup" : "",
		"dest" : "",
		"subject" : "",
		"remarks" : ""
	}
	
	$scope.InviteDateImage1 = 'img/late2.png';
	$scope.InviteDateImage2 = 'img/now1.png';
	
	$scope.ChangeDate = function(type)
	{
		$scope.OrderTime = type;
		
		if (type == 0)
		{
			$scope.InviteDateImage1 = 'img/late2.png';
			$scope.InviteDateImage2 = 'img/now1.png';			
		}
		else
		{
			$scope.InviteDateImage1 = 'img/late1.png';
			$scope.InviteDateImage2 = 'img/now2.png';			
		}
	}
	
	
	$scope.sendRequest = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
		if ($scope.fields.pickup =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת איסוף',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}

		
		else if ($scope.fields.dest =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת פיזור',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.fields.subject =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא אופן הבקשה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		
	
		else if ($scope.fields.remarks =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא הערות',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
		}

		else
		{
			request_data = 
			{
				"user" : $localStorage.userid,
				"name" : $localStorage.name,
				"phone" : $localStorage.phone,
				"email" : $localStorage.email,				
				"pickup" : $scope.fields.pickup,
				"dest" : $scope.fields.dest,
				"subject" : $scope.fields.subject,
				"remarks" : $scope.fields.remarks,
				"send" : 1 
			}					
			$http.post($rootScope.Host+'/send_request.php',request_data).success(function(data)
			{
					$ionicPopup.alert({
					 title: 'תודה , '+ $localStorage.name+ ' פניתך התקבלה בהצלחה , נחזור אליך בהקדם.',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });	
				   
				   $scope.fields.pickup = '';
				   $scope.fields.dest = '';
				   $scope.fields.email = '';
				   $scope.fields.subject = '';
				   $scope.fields.remarks = '';
				   
				   window.location.href = "#/app/main";
			});			
		}			
	}
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	

			$scope.TotalPrice = $scope.DeliveryPrice+$scope.BasketPrice;

	}

	$scope.updateBasket();

})


.controller('DetailsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$ionicLoading) {

	$scope.host = $rootScope.Host;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.TypeId = $stateParams.Type;
	$scope.ProductId = $stateParams.ItemId;
	$scope.CatagoryId = $stateParams.CatagoryId;
	//alert ($scope.CatagoryId);
	$scope.BasketPrice = 0;
	$scope.Size = "";
	$scope.Color = "";
	$scope.Subject = "";
	$scope.colorsArray = [];
	$scope.sizesArray = [];
	$scope.SubjectArray = [];
	$scope.controllerName = "DetailsCtrl";
	$scope.PrdQuan = "1";

	
	//alert ($scope.ProductId);
	
	$ionicLoading.show({
      template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
    });
	

  $scope.$on('imageloaded', function(events, args){
	$ionicLoading.hide();
  })

	
	$rootScope.$ionicGoBack = function() {
		//alert (123);
		if ($state.current.name == "app.details")
		{
			if ($scope.CatagoryId == "-1")
			{
				window.location.href = "#/app/basket";
			}
			else
			{
				window.location.href = $rootScope.productPage+$scope.CatagoryId;
			}			
		}
		else
		{
			window.location.href = "#/app/main";
		}

		
		
	};  


	$scope.$on('$destroy', function() {
		//$scope.ProductData.quan = 7;
		//alert ($scope.CatagoryId);
		//var oldSoftBack = $rootScope.$ionicGoBack;
		 //$rootScope.$ionicGoBack = oldSoftBack;
		 //alert (123);
	});

	
	$scope.Settings = function()
	{
		$scope.isAlert = 0;
//		console.log("Flag2")
		if ($scope.CatagoryId == "-1")
			$scope.ProductData = $rootScope.BasketArray[$scope.ProductId];
		else
		{
			//$scope.ProductData = $rootScope.currentArray[$scope.ProductId];
			for(i=0;i<$rootScope.AllProducts.length;i++)
			{
				//console.log("Len : " + $rootScope.AllProducts[i].index + " : "  +  $scope.ProductId)
				if($rootScope.AllProducts[i].index == $scope.ProductId)
				{
					$scope.ProductData = $rootScope.AllProducts[i];
					//console.log("Quan = " , $scope.ProductData)
				}
			}
		}
		
		//alert ($scope.ProductData.minorder)
		if ($scope.ProductData.quan > 0)
		{
			$scope.PrdQuan = $scope.ProductData.quan;
			//$scope.ProductData.quan = parseInt($scope.ProductData.minorder)+1;
			
		}
		else
		{
			$scope.PrdQuan = 1;
			//$scope.ProductData.quan = $scope.ProductData.quan;
		}
			
			
		if ($scope.ProductData.color)
		{
			$scope.colorsArray = $scope.ProductData.color;
			$scope.isAlert = 1;
		}
		
		if ($scope.ProductData.size)
		{
			$scope.sizesArray = $scope.ProductData.size;
			$scope.isAlert = 1;
		}
	
		if ($scope.ProductData.subjects)
		{
			$scope.SubjectArray = $scope.ProductData.subjects;
			$scope.isAlert = 1;
		}
	}
	
	$scope.Settings();
	
	
	//alert ($scope.ProductData.minorder)
	
	
	



	
//	console.log("ProductData",$scope.ProductData)
	
	$scope.changeQuan = function(type)
	{
		if (type == 1)
		{
			console.log("q1:" , $scope.ProductData.quan)
			$scope.ProductData.quan = String(parseInt($scope.ProductData.quan)+1);
			console.log("q2: ",$scope.ProductData.quan)
			$scope.PrdQuan = $scope.ProductData.quan;
		}
		else
		{
			if ($scope.ProductData.quan > $scope.ProductData.minorder)
			{
				$scope.ProductData.quan = String(parseInt($scope.ProductData.quan)-1);
				$scope.PrdQuan = $scope.ProductData.quan;
			}
			else
			{
				$ionicPopup.alert({
				 title: 'לא ניתן להזמין פחות ממינימום הזמנה '+$scope.ProductData.minorder+' פריטים',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });	
			}
			
		}
	}
	
	$scope.changeColor = function(type,index)
	{
		for(var j=0;j< $scope.colorsArray.length;j++)
		{
			//$scope.colorsArray[j].selected = "0";
			$scope.ProductData.color[j].selected = "0";
		}
		
		//$scope.colorsArray[index].selected = "1";
		$scope.ProductData.color[index].selected = "1";
		$scope.Color = type;	
		//console.log("ProductData" , $scope.ProductData)
		
		
	}
	


	$scope.changeSize = function(type,index)
	{
		for(var g=0;g< $scope.sizesArray.length;g++)
		{
			//$scope.sizesArray[g].selected = "0";
			$scope.ProductData.size[g].selected = "0";
		}
			
		//$scope.sizesArray[index].selected = "1";
		$scope.ProductData.size[index].selected = "1";
		$scope.Size = type;	
		
	}
	
	$scope.changeSubject = function(type,index)
	{
		
		for(var s=0;s< $scope.SubjectArray.length;s++)
		{
			//$scope.colorsArray[s].selected = "0";
			$scope.ProductData.subjects[s].selected = "0";
		}
		
		//$scope.colorsArray[index].selected = "1";
		$scope.ProductData.subjects[index].selected = "1";

		
		$scope.Subject = type;	
	}

	
	if ($scope.CatagoryId == -1)
	{
		console.log("Flag1")
		if ($scope.colorsArray[0])
		{
			for(var s=0;s< $scope.colorsArray.length;s++)
			{
				if ($scope.colorsArray[s].selected == 1)
				{
					$scope.Color = $scope.colorsArray[s].index;
				}						
			}
		}
		
		
		if ($scope.sizesArray[0])
		{
			for(var s=0;s< $scope.sizesArray.length;s++)
			{
				if ($scope.sizesArray[s].selected == 1)
				{
					$scope.Size = $scope.sizesArray[s].index;
				}						
			}
		}


		if ($scope.SubjectArray[0])
		{
			for(var s=0;s< $scope.SubjectArray.length;s++)
			{
				if ($scope.SubjectArray[s].selected == 1)
				{
					$scope.Subject = $scope.SubjectArray[s].index;
				}						
			}
		}
	}
	
	$scope.resetProduct = function()
	{

		//$scope.selectedColors = 0;
		//$scope.selectedSizes = 0;
		//$scope.selectedOptions = 0;	

		
		if ($scope.colorsArray[0])
		{
			for(var j=0;j< $scope.colorsArray.length;j++)
			{
				$scope.ProductData.color[j].selected = "0";
			}	
		}
		


		if ($scope.sizesArray[0])
		{
			for(var j=0;j< $scope.sizesArray.length;j++)
			{
				$scope.ProductData.size[j].selected = "0";
			}			
		}			
		
		
		if ($scope.SubjectArray[0])
		{
			for(var j=0;j< $scope.SubjectArray.length;j++)
			{
				$scope.ProductData.subjects[j].selected = "0";
			}			
		}			
		
		$scope.Size = "";
		$scope.Color = "";
		$scope.Subject = "";
	}
	
	
	$scope.CartOptions = function() 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'מוצר נוסף לעגלה בהצלחה , האם ברצונך להמשיך לסל קניות?',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'המשך בקניות',
		type: 'button-positive',
		onTap: function(e) { 
		  window.location.href = "#/app/products_template2/"+$scope.CatagoryId;;
		}
	   },
	   {
		text: 'הוסף מוצר נוסף',
		type: 'button-calm',
		onTap: function(e)
		{ 
		 	$scope.resetProduct();
		}

	   },
	   ]
	  });
	
	};	
	
	
	$scope.changeCheckBox = function(index,item,state)
	{
		if (state == 1)
		{
			item.isClicked = 0;
			item.quan = "1";
		}		
		else
		{
			item.isClicked = 1;
			//$scope.updateBasket();
		}
		console.log("basket new : ", $rootScope.BasketArray)	
		
		$scope.AddBasket(index,item);
		
	}
	
	$scope.checkOutBtn = function(item)
	{
		if ($scope.CatagoryId == -1)
			window.location.href = "#/app/basket"
		else
			$scope.AddBasket(item);
	}
	
	$scope.AddBasket = function(item)
	{
		item.quan = $scope.PrdQuan;
		
		$scope.ProductAdd = 0;
		$scope.ProductExists = 0;
		$scope.BasketPrice = 0;
	
		//$scope.selectedColors = 0;
		//$scope.selectedSizes = 0;
		//$scope.selectedOptions = 0;	

	

		//validate colors
			if ($scope.colorsArray[0])
			{
				$scope.selectedColors = 0;
				//alert ($scope.ProductData.color.length);
				for(var i=0;i< $scope.ProductData.color.length;i++)
				{
					if ($scope.ProductData.color[i].selected == 1)
					{
						$scope.selectedColors = 1;
					}
				}
				
			}
			else
			{
				$scope.selectedColors = 1;
			}

			
			//validate sizes
			if ($scope.sizesArray[0])
			{
				$scope.selectedSizes = 0;
				//alert ($scope.ProductData.size.length);
				for(var i=0;i< $scope.ProductData.size.length;i++)
				{
					if ($scope.ProductData.size[i].selected == 1)
					{
						$scope.selectedSizes = 1;
					}
				}
			}
			else
			{
				$scope.selectedSizes = 1;
			}



			
			//validate options
			if ($scope.SubjectArray[0])
			{
				$scope.SelectedName = $scope.SubjectArray[0].subject_name;
				$scope.selectedOptions = 0;
				//alert ($scope.ProductData.subjects.length);
				for(var i=0;i< $scope.ProductData.subjects.length;i++)
				{
					if ($scope.ProductData.subjects[i].selected == 1)
					{
						$scope.selectedOptions = 1;
					}
				}
				
			}
			else
			{
				$scope.selectedOptions = 1;
			}
			

		
		if ($rootScope.BasketArray.length == 0)
		{
			$scope.ProductExists = 0;
			$scope.ProductAdd = 1;		
		}
		else
		{
			for(var i=0;i< $rootScope.BasketArray.length;i++)
			{
				if ($rootScope.BasketArray[i].index == item.index)
				{
					$scope.ProductExists = 1;
				}
				else
				{
					$scope.ProductExists = 0;
					$scope.ProductAdd = 1;					
				}
			}	
		}		

			if ($scope.ProductAdd == 1 || $scope.ProductExists == 1)
			{
				if ($scope.selectedColors == 0)
				{
					$ionicPopup.alert({
					 title: 'יש לבחור צבע',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });						
				}
				else if ($scope.selectedSizes == 0)
				{
					$ionicPopup.alert({
					 title: 'יש לבחור גודל',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });						
				}
				else if ($scope.selectedOptions == 0)
				{
					$ionicPopup.alert({
					 title: 'יש לבחור '+$scope.SelectedName,
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });						
				}				
				else
				{
					item.isClicked = 1;
					var newItem = angular.copy(item);
					$rootScope.BasketArray.push(newItem);
					$localStorage.BasketArray = $rootScope.BasketArray;	
					
					if($scope.isAlert == 1)
					$scope.CartOptions();
					else
					window.location.href = $rootScope.productPage+$scope.CatagoryId;

				}
					
			}
		
		$scope.updateBasket();
	}

	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}	
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	

			$scope.TotalPrice = $scope.DeliveryPrice+$scope.BasketPrice;

	}

	$scope.updateBasket();

	
	
})

.controller('PackageFormCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	
	$scope.OrderTime = 0;
	$scope.InviteDateImage1 = 'img/late2.png';
	$scope.InviteDateImage2 = 'img/now1.png';
	$scope.DeliveryPrice = 15;
	$scope.TotalPrice = 0;
	
	$scope.timeStamp = Date.now(); 

	var myDate = new Date();
	var month = myDate.getMonth() + 1;
	var day = myDate.getDate();
	
	if(day<10) {
		day='0'+day
	} 
	if(month<10) {
		month='0'+month
	} 
	//.toString().substr(2,2);
	$scope.prettyDate =(day +'-'+ month) +'-'+ myDate.getFullYear();
	

	$scope.fields = 
	{
		"pickup" : "",
		"dest" : $localStorage.address,
		"deliverytype" : "",
		"packagesize" : 6,
		"remarks" : "",
		"deliverydate" : $scope.prettyDate
	}

	
	$scope.$watch($scope.fields.pickup.id, function(value) 
	 {
		 if ($scope.fields.pickup.id)
		 {
//			 alert (55);
		 }
	 });

	$scope.autocompleteOptions = {
		componentRestrictions: { country: 'il' },
		//types: ['geocode']
	}

	$scope.pickupData = function()
	{
		console.log("pickup: ",$scope.fields.pickup);
		
		
		if ($scope.fields.pickup.id)
		{
			if ($scope.fields.pickup.geometry)
			{
				if ($scope.fields.pickup.geometry.access_points)
				{
					alert ("pickup lat : "+$scope.fields.pickup.geometry.access_points[0].location.lat+ " pickup lng: "+ $scope.fields.pickup.geometry.access_points[0].location.lng)
				}	
			}	
		}
	}

	$scope.destData = function()
	{
		console.log("dest: ",$scope.fields.dest);
		
		
		if ($scope.fields.dest.id)
		{
			if ($scope.fields.dest.geometry)
			{
				if ($scope.fields.dest.geometry.access_points)
				{
					alert ("dest lat : "+$scope.fields.dest.geometry.access_points[0].location.lat +" dest lng: "+ $scope.fields.dest.geometry.access_points[0].location.lng)
				}	
			}	
		}
	}

	
	$scope.ChangeDate = function(type)
	{
		$scope.OrderTime = type;
		
		if (type == 0)
		{
			$scope.InviteDateImage1 = 'img/late2.png';
			$scope.InviteDateImage2 = 'img/now1.png';			
		}
		else
		{
			$scope.InviteDateImage1 = 'img/late1.png';
			$scope.InviteDateImage2 = 'img/now2.png';			
		}
	}
	
	$scope.getPackageSizes = function()
	{
		
			$http.get($rootScope.Host+'/get_Packages.php').success(function(data)
			{
				$scope.packagesArray = data;
				//$rootScope.UsersArray = $scope.AdminOrders;
				//console.log("users : " , $scope.UsersArray)
			});		
	}
	
	$scope.getPackageSizes();	
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
		
		$scope.TotalPrice = $scope.DeliveryPrice+$scope.BasketPrice;
	}

	$scope.updateBasket();

	

})
.controller('OrdersCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.ActiveTab = 0;
	
	$scope.getUserOrders = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
			orders_data = 
			{
				"user" : $localStorage.userid
			}					
			
			$http.post($rootScope.Host+'/get_Orders.php',orders_data).success(function(data)
			{
				$scope.OrdersArray = data;
				$rootScope.UserOrders = $scope.OrdersArray;
				console.log("orders: " , $scope.OrdersArray)
			});		
	}
	
	$scope.getUserOrders();
	
	$scope.setActiveTab = function(type)
	{
		$scope.ActiveTab = type;
	}
	
	$scope.OrderStatus = function(type)
	{
		if (type == 0)
		{
			$scope.FormattedStatus = 'הזמנה חדשה';
		}
		if (type == 1)
		{
			$scope.FormattedStatus = 'הזמנה בטיפול';
		}
		if (type == 2)
		{
			$scope.FormattedStatus = 'הזמנה נשלחה';
		}
		if (type == 3)
		{
			$scope.FormattedStatus = 'הזמנה הושלמה';
		}		
		
		return $scope.FormattedStatus;	
	}
	
	$scope.FormattedDate = function(date)
	{
		$scope.split = date.split(" ");
		$scope.splitHour = $scope.split[1].split(":");
		$scope.DateFormat = $scope.split[0];
		$scope.splitDate = $scope.DateFormat.split("-");
		$scope.newDate = $scope.splitDate[2]+'/'+$scope.splitDate[1]+'/'+$scope.splitDate[0];
		$scope.newHour = $scope.splitHour[0]+':'+$scope.splitHour[1];
		
		return $scope.newDate+ ' '+  $scope.newHour2;

	}
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();

	

})

.controller('OrderDetailsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.ActiveTab = 0;
	$scope.orderIndex = $stateParams.ItemId;

	
	
	$scope.orderData = $rootScope.UserOrders[$scope.orderIndex];
	$scope.CartProducts = $scope.orderData.cart;
	
	console.log("order details: " , $scope.orderData)

	$scope.FormattedDate = function(date)
	{
		$scope.split = date.split(" ");
		$scope.splitHour = $scope.split[1].split(":");
		$scope.DateFormat = $scope.split[0];
		$scope.splitDate = $scope.DateFormat.split("-");
		$scope.newDate = $scope.splitDate[2]+'/'+$scope.splitDate[1]+'/'+$scope.splitDate[0];
		$scope.newHour = $scope.splitHour[0]+':'+$scope.splitHour[1];
		
		return $scope.newDate+ ' '+  $scope.newHour;

	}
	$scope.OrderStatus = function(type)
	{
		if (type == 0)
		{
			$scope.FormattedStatus = 'הזמנה חדשה';
		}
		if (type == 1)
		{
			$scope.FormattedStatus = 'הזמנה בטיפול';
		}
		if (type == 2)
		{
			$scope.FormattedStatus = 'הזמנה נשלחה';
		}
		if (type == 3)
		{
			$scope.FormattedStatus = 'הזמנה הושלמה';
		}		
		
		return $scope.FormattedStatus;	
	}

	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}

	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();

	
})

.controller('AdminOrdersCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {


	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.ActiveTab = 0;


	$scope.getUsers = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
			users_data = 
			{
				"user" : $localStorage.userid
			}					
			
			$http.post($rootScope.Host+'/get_Users.php',users_data).success(function(data)
			{
				$scope.UsersArray = data;
				//$rootScope.UsersArray = $scope.AdminOrders;
				//console.log("users : " , $scope.UsersArray)
			});		
	}
	
	$scope.getUsers();

	
	
	$scope.getAdminOrders = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
			orders_data = 
			{
				"user" : $localStorage.userid
			}					
			
			$http.post($rootScope.Host+'/get_Admin_Orders.php',orders_data).success(function(data)
			{
				$scope.AdminOrders = data;
				$rootScope.AdminOrdersArray = $scope.AdminOrders;
				
				
				for(var i=0;i< $scope.AdminOrders.length;i++)
				{
					$scope.AdminOrders[i].delivery = "0";
				}	

		
				console.log("admin orders: " , $scope.AdminOrders)
			});		
	}
	
	$scope.getAdminOrders();
	
	
	$scope.sendOrder = function(item,index)
	{
		$scope.deliveryName = '';
		
		for(var i=0;i< $scope.UsersArray.length;i++)
		{
			if ($scope.UsersArray[i].id == item.delivery)
			{
				$scope.deliveryName = $scope.UsersArray[i].name;
			}
		}	
		
		var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר העברת הזמנה מספר '+item.index+ ' לשליח בשם: '+$scope.deliveryName+' ?'

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			 
			send_data = 
			{
				"user" : $localStorage.userid,
				"order_id" : item.index,
				"deliver_id" :  item.delivery,
				"send" : 1
			}					
			console.log("send : " , send_data)
			$http.post($rootScope.Host+'/send_order.php',send_data).success(function(data)
			{
				$ionicPopup.alert({
				 title: 'תודה ,  '+ $localStorage.name+ ' הזמנה נשלחה בהצלחה לשליח',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });			
					
				item.status = 1;
				//$scope.AdminOrders[index].status = "1";
				  
			});	
		 } 
	   });

				
		//alert (item.delivery)
		console.log("order : ", item)
	}
	
	
	$scope.setActiveTab = function(type)
	{
		$scope.ActiveTab = type;
	}
	
	$scope.OrderStatus = function(type)
	{
		if (type == 0)
		{
			$scope.FormattedStatus = 'הזמנה חדשה';
		}
		if (type == 1)
		{
			$scope.FormattedStatus = 'הזמנה בטיפול';
		}
		if (type == 2)
		{
			$scope.FormattedStatus = 'הזמנה נשלחה';
		}
		if (type == 3)
		{
			$scope.FormattedStatus = 'הזמנה הושלמה';
		}		
		
		return $scope.FormattedStatus;	
	}
	
	$scope.FormattedDate = function(date)
	{
		$scope.split = date.split(" ");
		$scope.splitHour = $scope.split[1].split(":");
		$scope.DateFormat = $scope.split[0];
		$scope.splitDate = $scope.DateFormat.split("-");
		$scope.newDate = $scope.splitDate[2]+'/'+$scope.splitDate[1]+'/'+$scope.splitDate[0];
		$scope.newHour = $scope.splitHour[0]+':'+$scope.splitHour[1];
		
		return $scope.newDate+ ' '+  $scope.newHour;

	}
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();
	
	

})

.controller('DeliveryCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.ActiveTab = 0;



	
	$scope.getAdminOrders = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
			orders_data = 
			{
				"user" : $localStorage.userid
			}					
			
			$http.post($rootScope.Host+'/get_Delivery_Orders.php',orders_data).success(function(data)
			{
				$scope.deliveryOrders = data;
				$rootScope.deliveryOrdersArray = $scope.AdminOrders;
				
				/*
				for(var i=0;i< $scope.deliveryOrders.length;i++)
				{
					$scope.AdminOrders[i].delivery = "0";
				}	
				*/

		
				console.log("delivery orders: " , $scope.deliveryOrders)
			});		
	}
	
	$scope.getAdminOrders();
	
	
	$scope.orderRecived = function(item,index)
	{
		var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מסירת הזמנה מספר '+item.order[0].index+' ?'

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			 
			send_data = 
			{
				"user" : $localStorage.userid,
				"order_id" : item.order[0].index,
				//"deliver_id" :  item.delivery,
				"send" : 1
			}					
			console.log("send : " , send_data)
			$http.post($rootScope.Host+'/update_order.php',send_data).success(function(data)
			{
				$ionicPopup.alert({
				 title: 'תודה ,  '+ $localStorage.name+ ' הזמנה עודכנה בהצלחה',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });			
					
				item.status = 1;
				//$scope.AdminOrders[index].status = "1";
				  
			});	
			
			item.order[0].status = 3;
		 } 
	   });
		
	}

	
	$scope.setActiveTab = function(type)
	{
		$scope.ActiveTab = type;
	}
	
	$scope.OrderStatus = function(type)
	{
		if (type == 0)
		{
			$scope.FormattedStatus = 'הזמנה חדשה';
		}
		if (type == 1)
		{
			$scope.FormattedStatus = 'הזמנה בטיפול';
		}
		if (type == 2)
		{
			$scope.FormattedStatus = 'הזמנה נשלחה';
		}
		if (type == 3)
		{
			$scope.FormattedStatus = 'הזמנה הושלמה';
		}		
		
		return $scope.FormattedStatus;	
	}
	
	$scope.FormattedDate = function(date)
	{
		$scope.split = date.split(" ");
		$scope.splitHour = $scope.split[1].split(":");
		$scope.DateFormat = $scope.split[0];
		$scope.splitDate = $scope.DateFormat.split("-");
		$scope.newDate = $scope.splitDate[2]+'/'+$scope.splitDate[1]+'/'+$scope.splitDate[0];
		$scope.newHour = $scope.splitHour[0]+':'+$scope.splitHour[1];
		
		return $scope.newDate+ ' '+  $scope.newHour;

	}
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();
	
	

})

.controller('SelectStoreCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.ActiveTab = 0;
	$scope.stores = [];
	$scope.storeExist = "";
	
	$scope.fields = 
	{
		"store" : ""
	}
	
	$scope.getStores = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		get_data = 
		{
			"custumerid" : $localStorage.custumerid,
			"agentid" : $localStorage.agentid,
			"user" : $localStorage.userid
		}

		$http.post($rootScope.Host+'/get_user_stores.php',get_data).success(function(data)
		{
			$scope.stores = data;
			
			if ($scope.stores.length ==  0)
			{
				$scope.storeExist = "";
				$scope.fields.store = $localStorage.custumerid;
				//$scope.canCheckOut  = 1;
			}
			else
			{
				$scope.storeExist = 1;
				$rootScope.selectedStore = "";
			}
			
			//console.log("stores: " , $scope.stores)
		});			
	}

	$scope.getStores();
	
	$scope.saveStore = function()
	{
		if ($scope.storeExist)
		{
			if ($scope.fields.store)
			{
				$scope.selectStoreName = $scope.stores[$scope.fields.store].name;
				$scope.selectedStoreIndex = $scope.stores[$scope.fields.store].index;
				$scope.fields.store = $scope.selectedStoreIndex;
				//alert ($scope.fields.store)
				$rootScope.storeName = $scope.selectStoreName;				
			}
		}


		if ($scope.fields.store == "")
		{
			$ionicPopup.alert({
			 title: 'יש לבחור חנות',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else
		{
			//alert ($scope.fields.store);
			$rootScope.selectedStore = $scope.fields.store;
			//alert ($rootScope.selectedStore)
			$state.go('app.basket');
		}


		   
	}
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();	
	

})


.controller('PopularCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,BasketService,$ionicLoading,$ionicSideMenuDelegate,server,GetCustomersFactory) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host; 

	$scope.userType = $localStorage.usertype;

	$scope.fields = 
	{
		"store" : ""
	}
	
	
	$scope.getStores = function()
	{
		get_data = 
		{
			"custumerid" : $localStorage.custumerid,
			"agentid" : $localStorage.agentid,
			"user" : $localStorage.userid
		}
			
		
		 server.request('/get_user_stores.php',get_data)
		 .then(function(data)
		 {
			 if (data == '' || data == null ||data == undefined)
			 return;
			 
			 $scope.stores = data;
			 console.log("Data : " + data)
		 });
	 
		/*$http.post($rootScope.Host+'/get_user_stores.php',get_data).success(function(data)
		{
			$scope.stores = data;
			
			console.log("stores: " , $scope.stores)
		});	*/		
	}

	$scope.getStores();


	$scope.getProducts = function(storeid)
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		get_data = 
		{
			"storeid" : storeid,
			"user" : $localStorage.userid
		}

		$http.post($rootScope.Host+'/get_Popular.php',get_data).success(function(data)
		{
			$scope.popularProducts = data;
			
			for(var i=0;i< $scope.popularProducts.length;i++)
			{
				$scope.popularProducts[i].quan = "1";
			}	

			console.log("popularProducts: " , $scope.popularProducts);
		});			
	}

	
	
	$scope.showProducts = function()
	{
		$scope.getProducts($scope.fields.store);
	}
	
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();	
	
	

})

.filter('cutString_TitlePage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' ');
    };
})

.filter('toTrusted', function ($sce) 
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
})

.filter('stripslashes', function () {
    return function (value) 
	{
		if(value)
		{
			return (value + '')
			.replace(/\\(.?)/g, function(s, n1) {
			  switch (n1) {
				case '\\':
				  return '\\';
				case '0':
				  return '\u0000';
				case '':
				  return '';
				default:
				  return n1;
			  }
			});
		}
		else
		{
			return ("");
		}
    };
})

.directive('imageonload', function($ionicLoading,$rootScope) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                //alert('image is loaded');
				$rootScope.$broadcast('imageloaded')
            });
        }
    };
});


