 // Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','google.places','starter.factories'])

.run(function($ionicPlatform,$rootScope,$http,$ionicLoading,$localStorage) {
	
	
	$rootScope.Host = 'http://www.mystor.co.il/php/';
	$rootScope.basketId = '';
	$rootScope.Logo = $rootScope.Host + "uploads/042216-150419_logo.png"
	$rootScope.Template = 2;
	$rootScope.storesArray = [];
	$rootScope.catArray = [];
	$rootScope.selectedStore = "";
	$rootScope.storeName = "";
	$rootScope.positionTop = 0;
	
	$rootScope.BasketArray = [];
	$rootScope.SettingsArray = [];
	$rootScope.UserOrders = [];
	$rootScope.AdminOrdersArray = [];
	$rootScope.deliveryOrdersArray = [];
	$rootScope.customerId = 3;
	$rootScope.getCustomerById = "";
	
	//$rootScope.navTitle ='<img class="title-image" src='+$rootScope.Logo+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	
	$rootScope.loadOnce = 0;
	$rootScope.CatagoriesLoaded = 0;
	$rootScope.currentArray = [];
	$rootScope.AllProducts = [];
	$rootScope.Customers = [];
	//console.log("LocalHost Return : ")
	//Get Category 
	/*
	$http.get('http://localhost:8000/data').success(function(data)
	{
		//console.log("LocalHost Return11 : " , data)
	});	
	*/
	
	//get products
	/*$http.get($rootScope.Host+'/getProducts.php').success(function(data)
	{
		$rootScope.ProductsArray = data;
		
			for(var i=0;i< data.length;i++)
			{
				$rootScope.ProductsArray[i].isClicked = false;
				$rootScope.ProductsArray[i].quan = "1";
			}
			
			//$rootScope.BasketArray.push($rootScope.ProductsArray[0])
		//console.log('products: ',$scope.ProductsArray)
	});*/		

	
/*
	// get stores
	$http.get($rootScope.Host+'/getStores.php').success(function(data)
	{
		$rootScope.storesArray = data;	
		//console.log('stores: ',data);
	});		

*/
	
	// get settings (about)
	
	console.log("ss")
	
	// get settings (about)
	$http.get($rootScope.Host+'/getSettings.php').success(function(data)
	{
		$rootScope.SettingsArray = data;	
		//console.log('SettingsArray: ',$rootScope.SettingsArray);
	});	
	
	$rootScope.productPage = '';
	
	switch($rootScope.Template)
	{
		case 1:
			$rootScope.productPage = "#/app/products_template1/"
			break;
			
		case 2:
			$rootScope.productPage = "#/app/products_template2/"
			break;
	}
	
	
	 $rootScope.getCatagories = function()
	  {
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		data1 = 
		{
			"categoryId" : $localStorage.custumerid 
		}				
		$http.post($rootScope.Host+'/getCategory.php',data1)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			$rootScope.catArray = data;
			console.log("catagories: " , data)
			
			$rootScope.CatagoriesLoaded++;	
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});	
	  }
	  
  $ionicPlatform.ready(function() {
	  
	   var notificationOpenedCallback = function(jsonData) {
	  //alert (JSON.stringify(jsonData));
	  //alert (jsonData.additionalData.type);
	  

	  /*
	  if (jsonData.additionalData.type == "newmessage")
	  {
		   $rootScope.$broadcast('newmessage',jsonData.additionalData);
		   $rootScope.$broadcast('refreshcodes',jsonData.additionalData);
		   
		   if (jsonData.additionalData.codeid)
		   {
			   if (!jsonData.isActive)
			   {
				   window.location ="#/app/details/"+jsonData.additionalData.codeid;
			   }   
		   }
	  }
	*/
	  
  

	  
	  /*
	  if (jsonData.additionalData.type == "newappoinment")
	  {
		  $rootScope.pushRedirect = jsonData.additionalData.supplierid;
		  
			if ($rootScope.pushRedirect)
			{
				window.location ="#/app/ratings/"+$rootScope.pushRedirect;
				$rootScope.pushRedirect = "";
			}
	  }
	  */
	  
    console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
  };
  
  
   window.plugins.OneSignal.init("ec85d93a-631d-48c8-8d6b-d0e750914edf",
                                 {googleProjectNumber: "627358870772"},
                                 notificationOpenedCallback);
								 
								 
	 window.plugins.OneSignal.getIds(function(ids) {
	
  $rootScope.pushId = ids.userId;	
  $rootScope.CouponsArray = ""
  
  
  
  
  if ($rootScope.pushId)
  {
	  /*
	  if ($localStorage.userid =="")
	  {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			send_params = 
			{
				"user" : $localStorage.userid,
				"pushId" : $rootScope.pushId
				
			}
	

			$http.post($rootScope.Host+'/save_visitor.php',send_params)
			.success(function(data, status, headers, config)
			{

			})
			.error(function(data, status, headers, config)
			{

			});			  
	  }
	 
	 */

  }

	  
  
  //$scope.pushId = ids.userId;
	
	
   //alert (ids.userId)
   //alert (ids.pushToken);

				
  //console.log('getIds: ' + JSON.stringify(ids));
});

								 
  // Show an alert box if a notification comes in when the user is in your app.
  window.plugins.OneSignal.enableInAppAlertNotification(false);
  window.plugins.OneSignal.clearOneSignalNotifications();

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
  
  
			
			
			
			
			




})


 



.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	
  $ionicConfigProvider.backButton.previousTitleText(false).text('');

		
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })
	
	.state('app.intro', {
      url: '/intro',
      views: {
        'menuContent': {
          templateUrl: 'templates/intro.html',
          controller: 'IntroCtrl'
        }
      }
    })

    .state('app.forgot', {
      url: '/forgot',
      views: {
        'menuContent': {
          templateUrl: 'templates/forgot.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })
    .state('app.register', {
      url: '/register',
      views: {
        'menuContent': {
          templateUrl: 'templates/register.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })
	
	
    .state('app.updateprofile', {
      url: '/updateprofile',
      views: {
        'menuContent': {
          templateUrl: 'templates/updateprofile.html',
          controller: 'UpdateProfileCtrl'
        }
      }
    })

	
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })	
    .state('app.products', {
      url: '/products',
      views: {
        'menuContent': {
          templateUrl: 'templates/products.html',
          controller: 'ProductsCtrl'
        }
      }
    })
	
	
	.state('app.products_template1', {
      url: '/products_template1/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/products_template1.html',
          controller: 'ProductsCtrl'
        }
      }
    })
	.state('app.products_template2', {
      url: '/products_template2/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/products_template2.html',
          controller: 'ProductsCtrl'
        }
      }
    })
    .state('app.packages', {
      url: '/packages',
      views: {
        'menuContent': {
          templateUrl: 'templates/packages.html',
          controller: 'ProductsCtrl'
        }
      }
    })	
	
    .state('app.details', {
      url: '/details/:Type/:ItemId/:CatagoryId',
      views: {
        'menuContent': {
          templateUrl: 'templates/details.html',
          controller: 'DetailsCtrl'
        }
      }
    })	

	
    .state('app.basket', {
      url: '/basket',
      views: {
        'menuContent': {
          templateUrl: 'templates/basket.html',
          controller: 'BasketCtrl'
        }
      }
    })	

    .state('app.shops', {
      url: '/shops',
      views: {
        'menuContent': {
          templateUrl: 'templates/shops.html',
          controller: 'ShopsCtrl'
        }
      }
    })	

    .state('app.storemenu', {
      url: '/storemenu/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/store_menu.html',
          controller: 'StoreMenuCtrl'
        }
      }
    })	

	
    .state('app.extras', {
      url: '/extras/:StoreId/:ProductId',
      views: {
        'menuContent': {
          templateUrl: 'templates/extras.html',
          controller: 'ExtrasCtrl'
        }
      }
    })	
	



	.state('app.contact', {
	  url: '/contact',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/contact.html',
		  controller: 'ContactCtrl'
		}
	  }
	})		

	.state('app.about', {
	  url: '/about',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/about.html',
		  controller: 'AboutCtrl'
		}
	  }
	})		
	
	.state('app.coupon', {
	  url: '/coupon',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/coupons.html',
		  controller: 'CouponCtrl'
		}
	  }
	})		
	
	.state('app.coupondetails', {
    url: '/coupondetails/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/coupondetails.html',
		controller: 'CouponsDetailsCtrl'
      }
    }
  })
	

	.state('app.specialrequests', {
	  url: '/specialrequests',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/specialrequests.html',
		  controller: 'SpecialRequestsCtrl'
		}
	  }
	})		
	
	.state('app.packageform', {
	  url: '/packageform',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/packageform.html',
		  controller: 'PackageFormCtrl'
		}
	  }
	})		

	.state('app.orders', {
	  url: '/orders',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/orders.html',
		  controller: 'OrdersCtrl'
		}
	  }
	})		

	.state('app.selectstore', {
	  url: '/selectstore',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/selectstore.html',
		  controller: 'SelectStoreCtrl'
		}
	  }
	})	

	
	
	.state('app.admin', {
	  url: '/admin',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/admin.html',
		  controller: 'AdminOrdersCtrl'
		}
	  }
	})		
	
	.state('app.delivery', {
	  url: '/delivery',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/delivery.html',
		  controller: 'DeliveryCtrl'
		}
	  }
	})		


	.state('app.popular', {
	  url: '/popular',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/popular.html',
		  controller: 'PopularCtrl'
		}
	  }
	})	



    .state('app.orderdetails', {
      url: '/orderdetails/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/orderdetails.html',
          controller: 'OrderDetailsCtrl'
        }
      }
    })	

	
	
	
  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/intro');
});
